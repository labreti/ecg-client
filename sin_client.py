#!/usr/bin/python
import time
import math
import json
import sys,getopt

from websocket import create_connection

user="anonymous"
server="192.168.5.2:8000"

def main(argv):
  global user
  global server
  try:
    opts, args = getopt.getopt(argv,"hs:u:",["server=","user="])
  except getopt.GetoptError:
    print 'ws_client.py -s <server> -u <user>'
    sys.exit(2)
  for opt, arg in opts:
    if opt == '-h':
      print 'ws_client.py -h <server> -u <user>'
      sys.exit()
    elif opt in ("-s", "--server"):
      server = arg
    elif opt in ("-u", "--user"):
      user = arg
  print 'User is ', user
  print 'Server is ', server
  
  ws = create_connection("ws://"+server+"/in/"+user)
  #ws = create_connection("ws://dyno-mia.herokuapp.com/in")
#  ws.send(user)
  print("Connected...")
  n=0.0
  while True:
    newData={'x': round(n,2),'y': round(math.sin(2*math.pi*n),2)};	
    n+=0.0063
    ws.send(json.dumps(newData))
#    print("Sent")
#    result = ws.recv()
#    print ("Received '%s'" % result)
    time.sleep(0.004)
  ws.close()
  
if __name__ == "__main__":
   main(sys.argv[1:])
