#!/usr/bin/python
import time
import math
import json
import sys,getopt
import serial
from websocket import create_connection

user="anonymous"
server="ecg-dyno.herokuapp.com/"

def main(argv):
	global user
	global server
	
	try:
		opts, args = getopt.getopt(argv,"hs:u:",["server=","user="])
	except getopt.GetoptError:
		print 'ws_client.py -s <server> -u <user>'
		sys.exit(2)
	
	ser = serial.Serial('/dev/ttyACM0', 115200, timeout=2, xonxoff=True, rtscts=False,dsrdtr=True)
	ser.flushInput()
	ser.flushOutput()
	oldvalues=True
	
	for opt, arg in opts:
		if opt == '-h':
			print 'ws_client.py -h <server> -u <user>'
			sys.exit()
		elif opt in ("-s", "--server"):
			server = arg
		elif opt in ("-u", "--user"):
			user = arg
	print 'User is ', user
	print 'Server is ', server
  
	ws = create_connection("ws://"+server+"/in/"+user)
  #ws = create_connection("ws://dyno-mia.herokuapp.com/in")
#	time.sleep(2)
	print("Connected...")
#	ws.send(user)
	while True:  
		dataline = ser.readline().strip()
		data=dataline.split()
		if len(data) != 7:
			print("NOT 7")
			print(data)
			continue
		if not data[0].translate(None,':.').isdigit():
			print("NONDIGIT")
			print(data)
			continue
		hms=data[0].split(':')
		if len(hms) != 3:
			print("NOT 3")
			print(data)
			continue
		tx=float(hms[2])+60*(int(hms[1])+60*int(hms[0]))
		if tx < 1:
			oldvalues=False
		if oldvalues:
			print("OLD")
			print(data)
			continue
		"Time: {t:8.2f}".format(t=tx)
		sig=[]
		for d in data[1:6]:
			v=round((float(d)-512)/512,3)
			if v >= -1 and v <= +1:
				sig.append(v)
			else:
				sig.append(0)
		jsonData={'x': tx,'y': sig[0]}
		ws.send(json.dumps(jsonData))
	ws.close()
	
if __name__ == "__main__":
	main(sys.argv[1:])
	
