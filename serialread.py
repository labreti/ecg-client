import serial
import sys

ser = serial.Serial('/dev/ttyACM0', 115200, timeout=2, xonxoff=True, rtscts=False,dsrdtr=True)
ser.flushInput()
ser.flushOutput()
oldvalues=True

while True:
	dataline = ser.readline().strip()
	data=dataline.split()
	if len(data) != 7:
		print("NOT 7")
		print(data)
		continue
	if not data[0].translate(None,':.').isdigit():
		print("NONDIGIT")
		print(data)
		continue
	hms=data[0].split(':')
	if len(hms) != 3:
		print("NOT 3")
		print(data)
		continue
	tx=float(hms[2]+60*(hms[1]+60*hms[0]))
	if tx < 1:
		oldvalues=False
	if oldvalues:
		print("OLD")
		print(data)
		continue
	"Time: {t:8.2f}".format(t=tx)
	sig=[]
	for d in data[1:6]:
		v=round((float(d)-512)/512,3)
		if v >= -1 and v <= +1:
			sig.append(v)
		else:
			sig.append(0)
	jsondata={'x': tx,'y': sig[0]}
	print(jsondata)
