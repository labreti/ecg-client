# ECG over WebSocket project (client)#

The purpose of this project is to implement the transmission of an ECG across the Internet using open protocols and low cost devices.

We have used the WebSocket protocol to transmit the ECG signal, and Arduino with a ECG-Shield to fetch the ECG signal, a Raspberry Pi to implement the WebSocket client, a dyno in the Heroku public cloud to implement the server. The ECG is visible in real time using a browser, visiting the page produced by the server.

## Content ##

This repository contains the client-side software.

* the ecg\_client python script that receives the input from the Arduino and forwards it across the WebSocket
* the sin\_client python script, useful for testing, that sends across the WebSocket an (approx) 1Hz signal
* the requirements.txt file that contains python dependencies, to simplify installation
* the Arduino sketch is available from this [repository] (https://github.com/AugustoCiuffoletti/Olimex-EKG_plotter.git)

The software for the server is in a companion [repository] (https://bitbucket.org/labreti/ecg-server).

## How to install and operate the client ##

### Arduino part ###
We used an Arduino Genuino, clone of the Uno, and a EKG-EMG Olimex shield 

* Flash the Arduino with the sketch (you can do this also from the Raspberry, or use any PC with the Arduino IDE)
* Plug the EKG/EMG Olimex shield on the Arduino
* Place the electrodes as appropriate

### Raspberry part ###
We used a Raspberry Pi with WiFi connection and the Raspian 8 version installed.
* Install Python 2.7.9
* Install the required libraries (see "requirements.txt")
* Call the script ecg\_client.py. Synopsis:
```
python ecg_client.py [-h] [-u <userkey>] [-s <server URL>]
```
* Alternatively, call the sin\_client.py that does no require the Arduino, but produce a 1Hs signal, or view the raw data using the serialread.pl script.